# Envision Android Assignment

## Introduction


Envision Library is a feature within the Envision app that users a lot after they've scanned a document. It offers them the convenience of storing the scanned documents within the app in an accessible way. This task will be a simple re-implementation of that feature.

## Design


You'll find the design for this feature through this figma link:  https://www.figma.com/file/m2AMO1eRV6SqWje63WmY2a/Envision-App-Assignments?node-id=91%3A1011

Instructions on how to approach the UX of the Assignment is also provided on the figma page itself. 

## Technical Requirements

1. OCR integration can be done using Firebase MLKit.

2. Make sure that the app is entirely accessible using Talkback. If you're not familar with Android Accessibility, this is a good place to start: https://developer.android.com/codelabs/starting-android-accessibility

3. CameraX should be used for the camera implementation. 

4. Library files will only have to be saved locally for the time being. 

## Submission

This repo should be forked and submitted with the Assignment. The assignment itself doesn't have a hard time limit and shouldn't take more than a few hours to implement. 

Incase of any questions with regards to the assignment, please write to karthik@letsenvision.com 



