package com.app.envision.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.app.envision.R
import java.text.SimpleDateFormat
import java.util.*


class LibraryListAdapter(private var fileList: Array<String>) :
    RecyclerView.Adapter<LibraryListAdapter.MyViewHolder>()
{
    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.tv_item_name)
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.lib_row_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (fileList[position].contains("_envision.txt"))
            holder.title.text = txtFormatting(fileList[position])
        else
            holder.title.text = fileList[position]
    }

    override fun getItemCount(): Int {
        return fileList.size
    }

    private fun txtFormatting(input: String): String {
        var format = SimpleDateFormat(FILENAME_FORMAT_BEFORE)

        val newDate: Date = format.parse(input)

        format = SimpleDateFormat(FILENAME_FORMAT_AFTER)

        return format.format(newDate)
    }

    companion object{
        private const val FILENAME_FORMAT_BEFORE = "dd-MM-yy-HH:mm:ss"
        private const val FILENAME_FORMAT_AFTER = "dd/MM/yy HH:mm:ss"

    }
}