package com.app.envision.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import com.app.envision.R

 class Utils {

    companion object {
        lateinit var alertDialog: Dialog

        //show progress dialog while calling async operation
        public fun showProgressDialog(context: Context) {
            alertDialog = Dialog(context)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(true)
            alertDialog.setContentView(R.layout.progress_alert)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            alertDialog.show()
        }

        //hide progress dialog after finish execution
        public fun dismissProgressDialog() {
                if (alertDialog.isShowing) alertDialog.dismiss()
        }
    }
}