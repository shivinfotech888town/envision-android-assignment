package com.app.envision.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.envision.adapter.SectionsPagerAdapter
import com.app.envision.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpAdapter()
    }

    /**
     * This function have setup adapter on viewpager
     */
    private fun setUpAdapter() {
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        binding.viewPager.adapter = sectionsPagerAdapter
        binding.tabs.setupWithViewPager(binding.viewPager)

        //by default set item 1 as selected
        binding.viewPager.currentItem = 1
    }


    companion object {
        //layout binding
        private lateinit var binding: ActivityMainBinding
         fun setTabItemAsLibrary(){
            binding.viewPager.currentItem = 2
             LibraryFragment.setItems()
        }
    }

}