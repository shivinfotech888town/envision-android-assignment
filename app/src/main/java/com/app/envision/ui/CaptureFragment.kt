package com.app.envision.ui

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.app.envision.R
import com.app.envision.databinding.CaptureFragmentBinding
import com.app.envision.utils.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


class CaptureFragment : Fragment() {

    companion object {
        const val TAG = "CaptureFragment"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val FILENAME_FORMAT_FILENAME = "dd-MM-yy-HH:mm:ss"

        private const val PHOTO_EXTENSION = ".jpg"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0

        private const val PERMISSIONS_REQUEST_CODE = 10
        private val PERMISSIONS_REQUIRED = arrayOf(Manifest.permission.CAMERA)

        /** Convenience method used to check if all permissions required by this app are granted */
        fun hasPermissions(context: Context) = PERMISSIONS_REQUIRED.all {
            ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
        }
    }

    private lateinit var captureFragmentBinding: CaptureFragmentBinding
    private lateinit var safeContext: Context
    private lateinit var snackbar: Snackbar

    private lateinit var viewFinder: PreviewView
    private lateinit var ivCaptured: ImageView
    private lateinit var tvResult: TextView
    private lateinit var btnAction: AppCompatButton
    private lateinit var cvContent: CardView

    private lateinit var outputDirectory: File
    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var imageAnalyzer: ImageAnalysis? = null
    private var camera: Camera? = null
    private var cameraProvider: ProcessCameraProvider? = null

    val recognizer = TextRecognition.getClient()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        safeContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // inflate the layout and bind to the _binding
        captureFragmentBinding = CaptureFragmentBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        init()
        return captureFragmentBinding.root
    }

    /**
     * This function initialize views and output factory
     */
    private fun init() {
        viewFinder = captureFragmentBinding.previewCamera
        ivCaptured = captureFragmentBinding.ivCaptured
        tvResult = captureFragmentBinding.tvResult
        btnAction = captureFragmentBinding.btnAction
        cvContent = captureFragmentBinding.cvContent
        tvResult.movementMethod = ScrollingMovementMethod()

        btnAction.run {
            setOnClickListener {
                if (text == getString(R.string.camera_txt))
                    permissionChecks()
                else if (text == getString(R.string.capture_txt))
                    takePhoto()
                else if (text == getString(R.string.save_lib_txt))
                    saveTextToLibrary()
            }
        }

        outputDirectory = getOutputDirectory()

    }

    /**
     * This function show customised snackbar
     */
    private fun showSnackBar() {
        snackbar = Snackbar
            .make(
                captureFragmentBinding.root,
                getString(R.string.saved_txt),
                Snackbar.LENGTH_INDEFINITE
            )
            .setAction(getString(R.string.go_to_lib)) {
                snackbar.dismiss()

                visibilityAsStepProgress(3)

                MainActivity.setTabItemAsLibrary()
            }

        // customize snackbar
        val textView =
            snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(safeContext, R.color.black))

        snackbar.setActionTextColor(
            ContextCompat.getColor(
                safeContext,
                R.color.design_default_color_primary
            )
        )
        snackbar.view.setBackgroundColor(ContextCompat.getColor(safeContext, R.color.white));

        snackbar.show()

    }

    /** Initialize CameraX, and prepare to bind the camera use cases  */
    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(Runnable {
            // CameraProvider
            cameraProvider = cameraProviderFuture.get()

            // Build and bind the camera use cases
            bindCameraUseCases()

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    /** Declare and bind preview, capture and analysis use cases */

    private fun bindCameraUseCases() {
        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }
        Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")

        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        Log.d(TAG, "Preview aspect ratio: $screenAspectRatio")

        val rotation = viewFinder.display.rotation

        // CameraProvider
        val cameraProvider = cameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")

        // CameraSelector
        val cameraSelector = CameraSelector.Builder().build()

        // Preview
        preview = Preview.Builder()
            // We request aspect ratio but no resolution
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation
            .setTargetRotation(rotation)
            .build()

        // ImageCapture
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            // We request aspect ratio but no resolution to match preview config, but letting
            // CameraX optimize for whatever specific resolution best fits our use cases
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // ImageAnalysis
        imageAnalyzer = ImageAnalysis.Builder()
            // We request aspect ratio but no resolution
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture, imageAnalyzer
            )

            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(viewFinder.surfaceProvider)

            btnAction.text = getString(R.string.capture_txt)
        } catch (exc: Exception) {
            Log.e(TAG, "Use case binding failed", exc)
        }
    }

    /**
     *  Detecting the most suitable ratio for dimensions provided in @params by counting absolute
     *  of preview ratio to one of the provided values.
     *
     *  @param width - preview width
     *  @param height - preview height
     *  @return suitable aspect ratio
     */
    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }


    private fun getOutputDirectory(): File {
        val mediaDir = activity?.externalMediaDirs?.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists()) mediaDir else activity?.filesDir!!
    }


    /** This function checks permissions for camera and request if required else launch cameraX */
    private fun permissionChecks() {
        if (!hasPermissions(requireContext())) {
            // Request camera-related permissions
            requestPermissions(
                PERMISSIONS_REQUIRED,
                PERMISSIONS_REQUEST_CODE
            )
        } else {
            setUpCamera()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (PackageManager.PERMISSION_GRANTED == grantResults.firstOrNull()) {
                // Take the user to the success fragment when permission is granted
                Toast.makeText(context, "Permission granted, launching cameraX", Toast.LENGTH_SHORT)
                    .show()
                setUpCamera()
            } else {
                Toast.makeText(context, "Permission request denied", Toast.LENGTH_SHORT).show()
            }
        }
    }


    /** This function perform photo capture */
    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // Create timestamped output file to hold the image
        val photoFile = File(
            outputDirectory,
            SimpleDateFormat(
                FILENAME_FORMAT,
                Locale.US
            ).format(System.currentTimeMillis()) + PHOTO_EXTENSION
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        // Setup image capture listener which is triggered after photo has been taken
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(safeContext),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)

                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)

                    Log.e(TAG, "Image captured $savedUri")
                    displayCapturedImage(savedUri);
                    performOCR(savedUri);
                }
            })
    }

    /**
     * This function display captured image in imageview using Glide
     * @param savedUri - uri of captured image
     */
    private fun displayCapturedImage(savedUri: Uri?) {
        visibilityAsStepProgress(1)
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
        Glide.with(safeContext)
            .load(savedUri)
            .apply(requestOptions)
            .into(ivCaptured)
    }

    /**
     * This function performs OCR on inputImage created from URI
     *@param savedUri - uri of captured image
     */
    private fun performOCR(savedUri: Uri?) {
        var image: InputImage? = null
        try {
            image = InputImage.fromFilePath(context, savedUri)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        //show ocr progress dialog
        Utils.showProgressDialog(safeContext)

        recognizer.process(image)
            .addOnSuccessListener { visionText ->
                // Task completed successfully
                //dismiss progress dialog
                Utils.dismissProgressDialog()
                Log.e(TAG, "success" + visionText.text)
                tvResult.text = visionText.text
                visibilityAsStepProgress(2)
            }
            .addOnFailureListener { e ->
                Utils.dismissProgressDialog()
                // Task failed with an exception
                Log.e(TAG, "error" + e.localizedMessage)
            }
    }

    /**
     * This function sets view visibilities
     */
    private fun visibilityAsStepProgress(step: Int) {
        when (step) {
            1 -> {
                viewFinder.visibility = View.GONE
                btnAction.visibility = View.GONE
                ivCaptured.visibility = View.VISIBLE
            }
            2 -> {
                cvContent.visibility = View.GONE
                btnAction.visibility = View.VISIBLE
                tvResult.visibility = View.VISIBLE
                btnAction.text = getString(R.string.save_lib_txt)
            }
            3 -> {
                cvContent.visibility = View.VISIBLE
                viewFinder.visibility = View.VISIBLE
                ivCaptured.visibility = View.GONE
                btnAction.visibility = View.VISIBLE
                tvResult.visibility = View.GONE
                btnAction.text = getString(R.string.capture_txt)
                btnAction.setBackgroundResource(R.drawable.shape_rounded_corner)
            }
            else -> {
                //nothing or reset
            }
        }
    }

    /** This function save file as .txt in local storage */
    private fun saveTextToLibrary() {
        val simpleDateFormat = SimpleDateFormat(FILENAME_FORMAT_FILENAME)
        val format = simpleDateFormat.format(Date())

        try {
            val fileContents = tvResult.text.toString()
            safeContext.openFileOutput(format + "_envision.txt", Context.MODE_PRIVATE).use {
                it.write(fileContents.toByteArray())
            }
            btnAction.setBackgroundResource(R.drawable.shape_grey)

            showSnackBar()
        } catch (e: IOException) {
            Log.e("Exception", "File write failed: $e")
        }
    }
}