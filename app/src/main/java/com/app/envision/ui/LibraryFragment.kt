package com.app.envision.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.envision.adapter.LibraryListAdapter
import com.app.envision.databinding.LibraryFragmentBinding

class LibraryFragment : Fragment() {

    private lateinit var libraryFragmentBinding: LibraryFragmentBinding
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onAttach(context: Context) {
        super.onAttach(context)
        safeContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // inflate the layout and bind to the _binding
        libraryFragmentBinding = LibraryFragmentBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        init()
        return libraryFragmentBinding.root
    }

    /**
     * This function initializing views
     */
    private fun init() {
        tvNoData = libraryFragmentBinding.tvNodata
        rvFileLists = libraryFragmentBinding.rvLibFiles
        linearLayoutManager = LinearLayoutManager(safeContext)
        rvFileLists.layoutManager = linearLayoutManager

        setItems()
    }

    companion object {
        private lateinit var safeContext: Context
        private lateinit var rvFileLists: RecyclerView
        private lateinit var tvNoData: TextView

        //fetch locally saved files and set to adapter
        fun setItems() {
            // we can do this operation using viewmodel as well and observe in fragment, avoiding to creat extra code/classes

            var filesToShow: Array<String> = arrayOf()

            var files: Array<String> = safeContext.fileList()

            if (files.isNotEmpty()) {
                val iterator: Iterator<String> = files.toList().iterator()

                //fileList() return two default firebase files
                // performing iteration to add only lib specific files

                iterator.forEach(fun(ste: String?) {
                    if (ste!!.contains("_envision.txt")) {
                        filesToShow += ste
                    }
                })
                if (filesToShow.isNotEmpty()) {
                    tvNoData.visibility = View.GONE
                    val adapterLibItems = LibraryListAdapter(filesToShow)
                    rvFileLists.adapter = adapterLibItems
                }
            } else {
                //we can show empty UI if required}
                tvNoData.visibility = View.VISIBLE
            }
        }
    }
}